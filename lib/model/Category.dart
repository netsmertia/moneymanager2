


enum CategoryType {
  income,
  expense,
  account
}
class Category {
  static final db_table_name = "categories";
  static final db_id = "_id";
  static final db_category_name = "category_name";
  static final db_category_type = "category_type";

  int id;
  String categoryName;
  CategoryType categoryType;
  double category_total;
  Category({
    this.id = 0,
    this.categoryName,
    this.categoryType,
    this.category_total = 0.0
  });

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      db_category_name: categoryName,
      db_category_type: categoryType.toString(),
    };
    if (id != null && id != 0) {
      map[db_id] = id;
    }
    return map;
  }


  Category.fromMap(Map map) {
    id = map[db_id] == null ? 0 : map[db_id];
    categoryName = map[db_category_name];
    categoryType = CategoryType.values.firstWhere((e) => e.toString() == map[db_category_type]);
    category_total = map['category_total'] != null ? map['category_total'].toDouble() : 0.0;
  }

  @override 
  String toString() {
    return "id $id, categoryName $categoryName, categoryType ${categoryType.toString()}, total ${category_total}";
  }

}