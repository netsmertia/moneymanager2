import 'package:flutter/foundation.dart' show required;
import "./Category.dart";


enum RecordType {
  income,
  expense,
  transfer
}

class Record {
  static final String db_table_name = "records";
  static final db_id = "_id";
  static final db_title = "title";
  static final db_record_date = "record_date";
  static final db_amount = "amount";
  static final db_from_wallet = "from_wallet";
  static final db_to_wallet = "to_wallet";
  static final db_category_id = "category_id";
  static final db_record_type = "record_type";
  static final db_created_at = "created_at";
  static final db_updated_at = "updated_at";


  String title, fromWallet, toWallet;
  int id;
  double amount;
  RecordType recordType;
  Category category;
  DateTime recordDate;
  DateTime createdAt;
  DateTime updatedAt;

  Record({
    this.title,
    this.id = 0,
    this.amount = 0.0,
    this.recordDate,
    @required this.category,
    @required this.fromWallet,
    this.toWallet = "",
    this.recordType = RecordType.expense,
    this.createdAt,
    this.updatedAt
  }) {
    if (this.recordDate == null) {
      DateTime t = DateTime.now();
      this.recordDate = DateTime(t.year, t.month, t.day);
    }

    if (this.createdAt == null) {
      this.createdAt = DateTime.now();
    }
    
    if (this.updatedAt == null) {
      this.updatedAt = DateTime.now();
    }

  }

  Record.fromMap(Map map) {
    id = map[db_id];
    category = new Category();
    title = map[db_title];
    amount = double.parse(map[db_amount].toString());
    recordDate = DateTime.parse(map[db_record_date]);
    createdAt = DateTime.parse(map[db_created_at]);
    updatedAt = DateTime.parse(map[db_updated_at]);
    fromWallet = map[db_from_wallet];
    recordType = RecordType.values.firstWhere((e) => e.toString() == map[db_record_type]);
    toWallet = "";
  }
  @override
  String toString() {
    return "id  $id, title $title, amount $amount, transactionDate $recordDate, fromWallet $fromWallet, category ${category.toString()}, createdAt ${createdAt.toString()} \n";
  }


  Map<String, dynamic> toMap() {
    
    Map<String, dynamic> map = {
      db_title: title,
      db_amount: amount,
      db_category_id: category.id,
      db_from_wallet: fromWallet,
      db_to_wallet: toWallet,
      db_record_date: recordDate.toIso8601String(),
      db_record_type: recordType.toString(),
      db_created_at: createdAt.toIso8601String(),
      db_updated_at: updatedAt.toIso8601String(),
    };
    if (id != null && id != 0) {
      map[db_id] = id;
    }
    return map;
  }

  
}
