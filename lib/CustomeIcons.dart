
import 'package:flutter/widgets.dart';

class CustomeIcons {
  CustomeIcons._();

  static const _kFontFam = 'CustomeIcons';

  static const IconData money = const IconData(0xf0d6, fontFamily: _kFontFam);
  static const IconData euro = const IconData(0xf153, fontFamily: _kFontFam);
  static const IconData pound = const IconData(0xf154, fontFamily: _kFontFam);
  static const IconData dollar = const IconData(0xf155, fontFamily: _kFontFam);
  static const IconData rupee = const IconData(0xf156, fontFamily: _kFontFam);
  static const IconData yen = const IconData(0xf157, fontFamily: _kFontFam);
  static const IconData rouble = const IconData(0xf158, fontFamily: _kFontFam);
  static const IconData won = const IconData(0xf159, fontFamily: _kFontFam);
  static const IconData bitcoin = const IconData(0xf15a, fontFamily: _kFontFam);
}
