import "package:sqflite/sqflite.dart";
import "package:flutter/foundation.dart" show required;
import '../model/Record.dart';
import '../model/Category.dart';
import "./database.dart";
import "dart:async";

class Repository {
  
  DatabaseHelper dbh;
  Repository() {
    dbh = new DatabaseHelper();
  }

  Future<Record> insertRecord(Record record) async {
    await _open();

    record.id = await dbh.db.insert(Record.db_table_name, record.toMap());
    return record;
  }

  Future<Record> insertRecords(List<Record> records) async {
    await _open();
    return await dbh.db.transaction((tnx) async {
      records.forEach((r) async {
        await tnx.insert(Record.db_table_name, r.toMap());
      });
    });
  }

  updateRecord(Record record) async {
    await _open();
    return await dbh.db.update(Record.db_table_name, record.toMap(), where: "${Record.db_id} = ?", whereArgs: [record.id]);
  }


  Future<List<Record>> getRecords({int limit, int offset, String where, List whereArgs, String orderBy }) async {
    await _open();
    List<Map> records = await dbh.db.query(
                          Record.db_table_name, 
                          limit: limit, 
                          offset: offset,
                          orderBy: orderBy,
                          where: where,
                          whereArgs: whereArgs,
                        );
    // get all cat ids of records
    Set ids = new Set();
    records.forEach((r) => ids.add(r[Record.db_category_id]));
    //get all cats where in ids
    List<Category> cats = await getCategoryWhereIn(column: Category.db_id, args: ids.toList());
    
    //create Record object with category object inserted
    List<Record> recordsList = new List();
    records.forEach((record) {
      Record r = Record.fromMap(record);
      r.category = cats.firstWhere((e) => e.id == record[Record.db_category_id]);
      recordsList.add(r);
    });
    return recordsList;
  }

  Future<List<Record>> getRecentRecords({int limit = 5}) async {
    return await getRecords(limit: limit, orderBy: "${Record.db_created_at} desc");
  }

  Future<List<Record>> getRecentRecordsBetween(DateTime startDate, DateTime endDate, {int limit = 5 }) async {
    return await getRecords(where: "${Record.db_record_date} between ? and ?", whereArgs: [startDate.toIso8601String(), endDate.toIso8601String()], limit: limit, orderBy: "${Record.db_created_at} desc");
  }




  Future<List> rawQuery(String query) async {
    await _open();
    return await dbh.db.rawQuery(query);
  }

  Future execute(String query, [List args]) async{
    await _open();
    return await dbh.db.execute(query, args);
  }

  insertCategories(List<Category> categories) async {
    await _open();
    return await dbh.db.transaction((tnx) async {
      for (Category c in categories) {
        tnx.insert(Category.db_table_name, c.toMap());
      }
    });
  }

  Future<List<Category>> getCategories ({String where, List wehreArgs}) async{
    await _open();
    List<Category> categories = new List();
    List data = await dbh.db.query(Category.db_table_name, where: where, whereArgs: wehreArgs);
    for (Map map in data) {
      categories.add(new Category.fromMap(map));
    }
    return categories;
  }

  Future<List<Category>> getCategoryWhereIn({String column, List args}) async {
    return await getCategories(
        where: "$column in ( " + List.generate(args.length, (_) => "?").join(", ") + ")",
        wehreArgs: args.toSet().toList(),
      );
  }

  Future<Category> getCategoryById(int id) async {

    List<Category> category = await getCategories(where: "${Category.db_id} = ?", wehreArgs: [id]);

    if (category.length > 0) {
      return category.elementAt(0);
    }

    return new Category();
  }

Future<List<Category>> getCategoriesWithTotal({DateTime startDate, DateTime endDate}) async {
  await _open();
  String query = "select *, sum(amount) as category_total " 
                  "from ${Category.db_table_name} left join ${Record.db_table_name} "
                  "on ${Category.db_table_name}.${Category.db_id} = ${Record.db_table_name}.${Record.db_category_id} ";

  query = startDate != null && endDate != null ? 
          query + "where ${Record.db_record_date} between '${startDate.toIso8601String()}' and '${endDate.toIso8601String()}' " 
          : query;
  query = query + "group by ${Category.db_table_name}.${Category.db_id} order by category_total desc";

  List<Category> categories = new List();
  List cats = await dbh.db.rawQuery(query);

  cats.forEach((c) => categories.add(new Category.fromMap(c)));
  print(cats.toString());

  return categories;
}

  _open() async {
    if (dbh.db == null) {
      await dbh.open();    
    }     
  }
}