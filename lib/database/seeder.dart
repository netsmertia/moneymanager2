import "./Repository.dart";
import "../model/Category.dart";
import "../model/Record.dart";
import 'dart:math';

class Seeder  {

  Repository repo;

  Seeder(Repository repo) {
    this.repo = repo;
  }  

  categorySeeder () async {
    List<Category> categories = new List();
    categories.add(new Category(categoryName: "Salary", categoryType: CategoryType.income));
    categories.add(new Category(categoryName: "Groceory", categoryType: CategoryType.expense));
    categories.add(new Category(categoryName: "Medical", categoryType: CategoryType.expense));
    categories.add(new Category(categoryName: "Fruits", categoryType: CategoryType.expense));
    categories.add(new Category(categoryName: "Household", categoryType: CategoryType.expense));
    categories.add(new Category(categoryName: "Rent", categoryType: CategoryType.income));
    categories.add(new Category(categoryName: "Bills", categoryType: CategoryType.expense));
    categories.add(new Category(categoryName: "Childern", categoryType: CategoryType.expense));
    categories.add(new Category(categoryName: "Fast food", categoryType: CategoryType.expense));
    await repo.execute("delete from ${Category.db_table_name}");
    await repo.insertCategories(categories);
    print('category seedind done');
  }


  recordSeeder () async {
    List<Category> categoryList =  await repo.getCategories();
    List<Record> records = new List();
    for (int i = 0; i < 20; i++) {
      records.add( new Record(
        title: "milk",
        amount: 500.0,
        category: categoryList[Random().nextInt(categoryList.length)],
        fromWallet: "cash",
        recordDate: DateTime(2016, 4, 6),
      ));
    }
    repo.insertRecords(records);
  }
}