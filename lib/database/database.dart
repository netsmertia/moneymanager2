import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'dart:io';
import 'dart:async';
import 'package:synchronized/synchronized.dart';
import '../model/Record.dart';
import '../model/Category.dart';


class DatabaseHelper {

  Database db;
  Lock lock = new Lock();
  final dbVersion = 5;

  final String defaultDbName = "ddatabase2.db";

  static final createRecordTable = "create table if not exists ${Record.db_table_name} ("
                                      "${Record.db_id} integer  primary key autoincrement, "
                                      "${Record.db_title} text, "
                                      "${Record.db_amount} number, "
                                      "${Record.db_from_wallet} text, "
                                      "${Record.db_to_wallet} text, "
                                      "${Record.db_record_date} text, "
                                      "${Record.db_record_type} text, "
                                      "${Record.db_category_id} number, "
                                      "${Record.db_created_at} text, "
                                      "${Record.db_updated_at} text "
                                    ")";
  static final createCategoryTable = "create table if not exists ${Category.db_table_name} ("
                                        "${Category.db_id} integer primary key autoincrement, "
                                        "${Category.db_category_name} text, "
                                        "${Category.db_category_type} text "
                                      ")";

  _dropTable (String tableName) => "DROP TABLE IF EXISTS $tableName";

  _onCreate(Database db, int version) async {
    await db.transaction((tnx) async{
      await tnx.execute(createRecordTable);
      await tnx.execute(createCategoryTable);
      //more table to create
      print("tables created"); 
    });
  }
  _onUpgrade(Database db, int oldVersion, int newVersion) async {
    await db.transaction((tnx) async{
      await tnx.execute(_dropTable(Record.db_table_name));
      await tnx.execute(_dropTable(Category.db_table_name));
      print("tables deleted");
      _onCreate(db, newVersion);
    });
  }
  open({String dbName = ''}) async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, dbName == '' ? defaultDbName : dbName);
    if (db == null) {
      await lock.synchronized(() async {
        if(db == null) {
          this.db = await openDatabase(path, version: dbVersion, 
            onCreate: _onCreate,
            onUpgrade: _onUpgrade,
          );
        }
      });
    }
  }




  Future close() async {
    if (db != null) {
      db.close();
    }
  }
}