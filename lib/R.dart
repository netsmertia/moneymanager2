import 'package:flutter/material.dart';
class R {
  static final double HPadding = 12.0;
  static final double VPadding = 12.0;
  static EdgeInsets HVPadding = EdgeInsets.symmetric(horizontal: HPadding, vertical: VPadding);
}