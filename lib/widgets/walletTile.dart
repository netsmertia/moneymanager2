import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';


class WalletTile extends StatelessWidget {
  final String title;
  final String subtitle;
  final String leadingText;
  final double inflow;
  final double outflow;
  final double balance;
  final Color leadingBackground;
  final GestureTapCallback onTap;

  WalletTile({
      Key key, 
      @required this.title, 
      @required this.subtitle, 
      @required this.leadingText, 
      @required this.inflow, 
      @required this.outflow, 
      @required this.balance,
      this.onTap,
      this.leadingBackground = Colors.green
    }): assert(title != null),
      // assert(subtitle != null),
      assert(leadingText != null),
      assert(inflow != null),
      assert(outflow != null),
      assert(balance != null),
    super(key: key);


  Widget build(BuildContext context) {
    this.leadingBackground.value;
    return Container(
      decoration: BoxDecoration(border: Border(top: BorderSide(color: Colors.grey[200]))),
      child: ListTile(
          leading: CircleAvatar(
            backgroundColor: this.leadingBackground,
            child: Text(this.leadingText, style: TextStyle(color: Colors.white)),
          ),
          title: Text(this.title, style: TextStyle(fontSize: 16.0)),
          subtitle: Text(this.subtitle?? '', style: TextStyle(fontSize: 12.0, fontStyle: FontStyle.italic, color: Colors.grey[400])),
          trailing: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Text('Inflow: ' + this.inflow.toStringAsFixed(2), style: TextStyle(color: Colors.grey[400], fontStyle: FontStyle.italic, fontSize: 12.0)),
              Text('Outflow: ' + this.outflow.toStringAsFixed(2), style: TextStyle(color: Colors.grey[400], fontStyle: FontStyle.italic, fontSize: 12.0)),
              Text('Balance: ' + this.balance.toStringAsFixed(2), style: TextStyle(color: Colors.green, fontStyle: FontStyle.italic, fontSize: 16.0)),
            ],
          ),
          onTap: this.onTap,
        )
      ); 
  }
}