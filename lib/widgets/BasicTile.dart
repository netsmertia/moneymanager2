import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:moneylover2/CustomeIcons.dart';


class BasicTile extends StatelessWidget {

  String title;
  String leading;
  String subtitle;
  String trailing;
  String subtrailing;
  Color trailingColor;
  GestureTapCallback onTap;
  

  BasicTile({
    Key key, 
    @required this.title,
    this.subtitle = '',
    @required this.trailing,
    this.subtrailing = '',
    this.trailingColor,
    this.onTap,
    @required this.leading,
  }): 
    assert(leading != null),
    assert(title != null),
    assert(trailing != null),
    super(key: key);


  Widget build(BuildContext context) {
    return Container(
        child: ListTile(
          onTap: onTap,
          leading: CircleAvatar(
              child: Text(leading != '' ? leading : 'U', style: TextStyle(color: Colors.white),), 
              backgroundColor: Colors.purple,),
          title: Text(title != '' ? title : 'Unknow Transaction', style: TextStyle(fontSize: 16.0),),
          subtitle: Padding(
            padding: const EdgeInsets.symmetric(vertical: 3.0),
            child: Text(this.subtitle, style: TextStyle(color: Colors.grey[400], fontStyle: FontStyle.italic, fontSize: 13.0)),
          ),
          trailing: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Icon(CustomeIcons.rupee, size: 16.0, color: trailingColor != null ? trailingColor : Colors.grey[200]),
                  Text(this.trailing, textAlign: TextAlign.end, style: TextStyle(fontSize: 18.0, color: trailingColor != null ? trailingColor : Colors.grey[500])),
                ],
              ),
              SizedBox(height: 4.0,),
              Text(this.subtrailing, style: TextStyle(color: Colors.grey[400],  fontSize: 13.0), textAlign: TextAlign.end,),
            ],
          ),
        ),
        decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.grey[200],))),
    );
  }
}
