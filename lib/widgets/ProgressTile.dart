import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:moneylover2/CustomeIcons.dart';

class ProgressTile extends StatelessWidget {

  String title;
  double percentage;
  String trailing;
  String subtrailing;
  ProgressTile({
    Key key, 
    
    @required this.title,
    this.percentage = 0.0,
    @required this.trailing,
    this.subtrailing = '',
  }): 
    assert(title != null),
    assert(trailing != null),
    super(key: key);

  Widget build(BuildContext context) {
    return Container(
        child: ListTile(
          leading: CircleAvatar(child: Text(title != "" ? title.substring(0, 1).toUpperCase() : 'U', style: TextStyle(color: Colors.white),), backgroundColor: Colors.red,),
          title: Text(this.title),
          subtitle: Stack(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 5.0),
                height: 7.0,
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(5.0),
                ),
              ),
              FractionallySizedBox(
                widthFactor: percentage,
                alignment: Alignment.centerLeft,
                child: Container(
                  constraints: BoxConstraints(maxWidth: 30.0),
                  height: 7.0, 
                  // width: 30.0,
                  margin: EdgeInsets.only(top: 5.0),
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5.0), color: Colors.green),
                )
              ),
            ],
          ),
          trailing: ConstrainedBox(
              constraints: BoxConstraints(maxWidth: 70.0),
                child: Padding(
                padding: const EdgeInsets.only(top: 2.0, bottom: 0.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Icon(CustomeIcons.rupee, size: 14.0,),
                        Text(this.trailing, textAlign: TextAlign.end, style: TextStyle( color: Colors.grey, fontSize: 16.0),),
                      ],
                    ),
                    SizedBox(height: 4.0,),
                    Text(this.subtrailing, textAlign: TextAlign.end, style: TextStyle(color: Colors.grey[400], fontStyle: FontStyle.italic, fontSize: 13.0)),
                  ],
                ),
              ),
          )
        ),
        decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.grey[200],))),
    );
  }
}
