import 'package:flutter/material.dart';

enum ActionTileLeadingType {
  image,
  icon,
  text
}
enum ActionTileType {
  text,
  input
}

class ActionTile extends StatelessWidget {

  ActionTileLeadingType leadingType;
  dynamic leading;
  bool isTwoLine;
  ActionTileType tileType;
  String title;
  String subtitle;
  GestureDragCancelCallback onTap;
  TextEditingController controller;
  
  ActionTile({
    this.leadingType = ActionTileLeadingType.icon,
    this.leading,
    this.isTwoLine = false,
    this.tileType = ActionTileType.text,
    this.title,
    this.subtitle,
    this.onTap,
    this.controller,
  })  : 
    // assert(leading != null),
    assert(title != null);



  _buildLeading() {
    switch(leadingType) {
      case ActionTileLeadingType.icon : 
          return Icon(leading is IconData ? leading : Icons.donut_large, color: Colors.green[700],size: 30.0);
      case ActionTileLeadingType.text : 
          String ch = "xx";
          if (leading is String) {
            ch = leading.substring(0, 1).toUpperCase();
          } else if (subtitle != null && subtitle != '') {
            ch = subtitle.substring(0, 1).toUpperCase();
          } else if (title != null && title != '') {
            ch = title.substring(0, 1).toUpperCase();
          } else {
            ch = "A";
          }
          return CircleAvatar(child: Text(ch), radius: 17.0,);
      case ActionTileLeadingType.image: 
        return Placeholder(fallbackWidth: 50.0, fallbackHeight: 30.0,);
    }
  }

  _buildCenter() {
    switch(tileType) {
      case ActionTileType.text: 
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(title, 
              style: TextStyle(
                fontSize: !isTwoLine ? 18.0 : 10.0,
                color: !isTwoLine ? Colors.grey[700]: Colors.grey[400],
              ),
            ),
            isTwoLine && subtitle != null ? Text(subtitle,
              style: TextStyle(
                fontSize: 18.0,
                color: Colors.grey[700],
              ),
            ) : Container(),
          ],
        );
      case ActionTileType.input : 
        return Flexible(
          child: TextField(
            controller: controller,
            style: TextStyle(
              fontSize: 18.0,
              color: Colors.grey[700],
            ),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 18.0),
              hintText: title,
              border: InputBorder.none,
            ),
          )
        );
    }
  }

  @override
  Widget build(BuildContext context) {
      return InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.only(left: 30.0, top: tileType == ActionTileType.input ? 0.0:  12.0, bottom: tileType == ActionTileType.input ? 0.0 : 12.0,), 
          child: Row(
            children: <Widget>[
              _buildLeading(),
              SizedBox(width: 25.0,),
              _buildCenter(),
            ],
          ),
        )
      );
  }
}