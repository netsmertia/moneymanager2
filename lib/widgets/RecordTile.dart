import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:moneylover2/model/Category.dart';
import 'package:moneylover2/model/Record.dart';
import 'package:moneylover2/widgets/BasicTile.dart';



class RecordTile extends StatelessWidget {

  RecordTile(this.record, [this.onTap]);
  
  Record record;
  GestureTapCallback onTap;

  Color _getColor(CategoryType type) {

    switch(type) {
      case CategoryType.income : 
        return Colors.green;
      case CategoryType.expense: 
        return Colors.pink[400];
      case CategoryType.account : 
      return Colors.grey;
    }

    return Colors.grey;
  }

  Widget build(BuildContext context) {
    return BasicTile(
      leading: record.category.categoryName.substring(0, 1).toUpperCase(),
      onTap: onTap,
      title: record.title != null && record.title.isNotEmpty ? record.title : 'Unknown',
      subtitle: record.category.categoryName  + " - " + record.fromWallet,
      subtrailing: DateFormat('dd MMM').format(record.recordDate),
      trailing: record.amount.toStringAsFixed(2),
      trailingColor: _getColor(record.category.categoryType),
    );
  }
}