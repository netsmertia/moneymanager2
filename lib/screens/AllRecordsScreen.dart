
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:moneylover2/database/Repository.dart';
import 'package:moneylover2/model/Record.dart';
import 'dart:async';

import 'package:moneylover2/widgets/RecordTile.dart';

class AllRecordsScreen extends StatefulWidget {

  final Repository repo;

  AllRecordsScreen(this.repo);

  createState() => AllRecordsScreenState();

}

class AllRecordsScreenState extends State<AllRecordsScreen> {

  List<Record> records = new List();
  DateTime filterDate = new DateTime.now();
  
  initState() {
    super.initState();
    _loadRecords();
  }

  _loadRecords() {
    widget.repo.getRecords(
        where : "${Record.db_record_date} between ? and ?",
        whereArgs: [DateTime(filterDate.year, filterDate.month, 1).toIso8601String(), DateTime(filterDate.year, filterDate.month + 1, 0).toIso8601String()],
        orderBy: "${Record.db_created_at} DESC"
      ).then((recordSet) {
      setState(() {
        records = recordSet;        
      });
    });
  }

  _addMonth() {
    setState(() {
      filterDate = new DateTime(filterDate.year, filterDate.month + 1, filterDate.day);      
      _loadRecords();
    });
    print(filterDate);
  }

  _subMonth() {
    setState(() {
      filterDate = new DateTime(filterDate.year, filterDate.month - 1, filterDate.day);      
      _loadRecords();
    });
    print(filterDate);
  }


  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('All Transactions'),
      ),
      body: Column(
        children: <Widget>[
          _buildMonthSelector(context),
          Divider(height: 1.0,),
          Expanded(
            child: ListView.builder(
              itemCount: records.length,
              itemBuilder: (context, index)  {
                return RecordTile(records.elementAt(index), () {});
              },
            ),
          ),
        ],
      )
    );
  }

  Widget _buildMonthSelector(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 6.0),
      child: Row(
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.chevron_left, size: 30.0, color: Colors.grey,),
            onPressed: _subMonth,
          ),
          Expanded(
            child: Text(
              DateFormat('MMM yyyy').format(filterDate),
              style: Theme.of(context).textTheme.title.copyWith(color: Colors.grey),
              textAlign: TextAlign.center,
            )
          ),
          IconButton(
            icon: Icon(Icons.chevron_right, size: 30.0, color: Colors.grey,),
            onPressed: _addMonth,
          ),
        ],
      ),
    );
  }
}