
import 'package:flutter/material.dart';
import 'package:moneylover2/database/Repository.dart';
import 'package:moneylover2/model/Category.dart';
import 'package:moneylover2/widgets/ProgressTile.dart';

class AllCategoriesScreen extends StatefulWidget {

  Repository repo;

  AllCategoriesScreen(this.repo);

  createState() => AllCategoriesScreenState();

}



class AllCategoriesScreenState extends State<AllCategoriesScreen> {

  DateTime filterDate = DateTime.now();
  Map<CategoryType, List<Category>> categories = new Map();

  initState() {
    super.initState();
    CategoryType.values.forEach((t) => categories.putIfAbsent(t, () => List<Category>()));
    _loadAllCategories();
  }
  _loadAllCategories() {
    widget.repo.getCategoriesWithTotal(
      startDate: DateTime(filterDate.year, filterDate.month, 1),
      endDate: DateTime(filterDate.year, filterDate.month + 1, 0),
    ).then((cats) {
      setState(() {
        print(cats.toString());
        cats.forEach((c) => categories[c.categoryType].add(c));
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    return DefaultTabController (
        length: 3,
        initialIndex: 1,
        child: Scaffold(
        appBar: AppBar(
          title: Text('All Categories'),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(child: Text('Income'),),
              Tab(child: Text('Expense'),),
              Tab(child: Text('Transfer'),),
            ],
          ),

        ),

        body: TabBarView(
          children: <Widget>[
            _buildTabView(CategoryType.income),
            _buildTabView(CategoryType.expense),
            _buildTabView(CategoryType.account),
            // Placeholder(),
            // Container(),
            // Placeholder(),
          ],
        )
      ),
    );
  }

  _buildTabView(CategoryType type) {
    List<Category> cats = categories[type];
    double total = cats.fold(0.0, (oldValue, c) => oldValue + c.category_total);
    if (cats.isEmpty) {
      return Center(
        child: Text('No records to show'),
      );
    }
    return ListView.builder(
      itemCount: cats.length,
      itemBuilder: (contex, index) {
        Category cat = cats.elementAt(index);
        return ProgressTile(
          title: cat.categoryName,
          percentage: cat.category_total / total,
          trailing: cat.category_total.toStringAsFixed(0),
          subtrailing: (cat.category_total / total * 100).toStringAsFixed(2) + "%",
        );
      },
    );
  }
}