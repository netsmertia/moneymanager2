import 'package:flutter/material.dart';
import 'package:moneylover2/model/Category.dart';
import 'package:moneylover2/database/Repository.dart';

class SelectCategoryScreen extends StatefulWidget {
  final Repository repo;
  final tabIndex;

  SelectCategoryScreen(this.repo, this.tabIndex);

  createState() => SelectCategoryScreenState();
}

class SelectCategoryScreenState extends State<SelectCategoryScreen> {

  // List<Category> categories;
  Map<CategoryType, List<Category>> categoriesMapState = new Map();
  Map<CategoryType, List<Category>> categoriesMap = new Map();


  initState() {
    super.initState();
    //add an empty list of category for each category type
    // CategoryType.values.forEach((e) => categoriesMapState.putIfAbsent(e, () => new List<Category>()));
    CategoryType.values.forEach((e) => categoriesMap.putIfAbsent(e, () => new List<Category>()));
    CategoryType.values.forEach((e) => categoriesMapState.putIfAbsent(e, () => new List<Category>()));
    _loadCategories();
  }

  _loadCategories() {
    widget.repo.getCategories().then((cats) {
      cats.forEach((cat) {
        categoriesMap[cat.categoryType].add(cat);
      });
      setState(() {
        categoriesMapState = categoriesMap;        
        print(categoriesMap.toString());
      });
    });
  }
  _onCategorySelect(BuildContext context, CategoryType type, int index)  {
    Category category = categoriesMapState[type].elementAt(index);
    Navigator.of(context).pop(category);
  }

  _buildCategoryList(CategoryType type) {
    List<Category> cats = categoriesMapState[type];
    return ListView.builder(
      itemCount: cats.length,
      itemBuilder: (context, index) => Column(
        children: <Widget>[
          ListTile(
            contentPadding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 18.0),
            leading: CircleAvatar(
              child: Text(
                cats.elementAt(index).categoryName.substring(0,1).toUpperCase(),
                style: TextStyle(color:Colors.white),
              ),
            ),
            title: Text(cats.elementAt(index).categoryName),
            onTap: () => _onCategorySelect(context, cats.elementAt(index).categoryType, index),
          ),
          Divider(height: 1.0,),
        ],
      ),
    );
  }


  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
      length: 3,
      initialIndex: widget.tabIndex,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Select Category'),
          bottom: new TabBar(
            tabs: <Widget>[
              Tab(child: Text('Income'),),
              Tab(child: Text('Expense'),),
              Tab(child: Text('Transfer'),),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
           _buildCategoryList(CategoryType.income),
           _buildCategoryList(CategoryType.expense),
           _buildCategoryList(CategoryType.account),
          ],
        )
      )
    );
  }
}