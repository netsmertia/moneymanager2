import 'package:flutter/material.dart';
import 'package:moneylover2/model/Category.dart';
import 'package:moneylover2/screens/AllCategoriesScreen.dart';
import 'package:moneylover2/screens/AllRecordsScreen.dart';
import 'package:moneylover2/screens/CreateAndEditRecordScreen.dart';
import 'package:moneylover2/widgets/RecordTile.dart';

import 'package:moneylover2/widgets/walletTile.dart';
import 'package:moneylover2/widgets/ProgressTile.dart';
import 'package:moneylover2/database/Repository.dart';
import 'package:moneylover2/database/seeder.dart';
import "package:moneylover2/model/Record.dart";
import "dart:math";

class HomeScreen extends StatefulWidget {
  final Repository repo;
  HomeScreen(
    this.repo
  );

  @override
  createState() => HomeScreenState();

}

class HomeScreenState extends State<HomeScreen> {

  List<Record> _recentRecords = new List();
  List<Category> _categories = new List();
  double totalSpend = 0.0;

  HomeScreenState() {
    // _seederInit();
  }
  initState() {
    super.initState();
    _getRecentTransactions();
    _getCategoryWiseTotal();
  }

  _seederInit() async {
    Seeder seeder = new Seeder(widget.repo);
    await seeder.categorySeeder();
    await seeder.recordSeeder();
  }

  _getRecentTransactions() {
    DateTime today = new DateTime.now();
    widget.repo.getRecentRecordsBetween(new DateTime(today.year, today.month, 1), new DateTime(today.year, today.month + 1, 0)).then((records) {
      setState(() {
        _recentRecords = records;       
      });
    });
  }
  
  _getCategoryWiseTotal() {
    DateTime today = new DateTime.now();
    widget.repo.getCategoriesWithTotal(
      startDate: DateTime(today.year, today.month, 1) , //start of month
      endDate: DateTime(today.year, today.month + 1, 0) // end of month by passing month + 1 and day 0
    ).then((cats) {
      double total = cats.fold(0.0, (p, n) => p+n.category_total);
      setState(() {
        _categories = cats;
        totalSpend = total;
      });
    });
  }

  _createRecord(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return CreateAndEditRecordScreen(widget.repo);
    })).then((record) {
      if (record != null) {
        _getRecentTransactions();
        _getCategoryWiseTotal();
      }
    });
  }

  _onRecordTap(BuildContext context, Record record) {
    Navigator.of(context).push( 
      MaterialPageRoute(builder: (context) => CreateAndEditRecordScreen(widget.repo, record: record,))
    ).then((record) {
      if (record != null) {
        _getRecentTransactions();
        _getCategoryWiseTotal();
      }
    });
  }

  _onRecordMoreTap(BuildContext context) {
    Navigator.of(context).push( 
      MaterialPageRoute(builder: (context) => AllRecordsScreen(widget.repo))
    );
  }
  
  _onCategoriesMoreTap(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(builder:  (context) => AllCategoriesScreen(widget.repo))
    );
  }

  Widget build (BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: Text('Money Lover'),
        ),
        body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
          children: <Widget>[
            _buildTopCard(context),
            _buildRecentTransactionCard(context),
            _buildCategoryWiseSpendingCard(context),
          ],
        )
      );
  }

  //TopCard
  Widget _buildTopCard(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(bottom: 16.0),
      child: Column(
        children: <Widget>[
          SizedBox(height: 16.0),
          _buildTotal(context),
          SizedBox(height: 16.0,),
          WalletTile(leadingText: 'C', title: 'Cash',  subtitle: 'Wallet', inflow: 3200.0, outflow: 1200.0, balance: 234.0, onTap: () {
          }, leadingBackground: Colors.orange,),
          WalletTile(leadingText: 'O', title: 'Online',  subtitle: 'Wallet', inflow: 3200.0, outflow: 1200.0, balance: 234.0, onTap: () {}, leadingBackground: Colors.teal,),
          _buildTopCardFooter(context),
        ],
      ),
    );
  }

  Widget _buildTotal(context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
       Text('Total Spendings', style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),),
       Text('15000.00', style: TextStyle(fontSize: 20.0, fontStyle: FontStyle.italic),),
      ],
    );
  }

  Widget _roundedButton(BuildContext context) {
    return Material(
      // decoration: BoxDecoration(color: Colors.green, borderRadius: BorderRadius.circular(30.0)),
      color: Colors.green[700],
      borderRadius: BorderRadius.circular(30.0),
      child: InkWell(
        onTap: () {_createRecord(context);},
        child: Container(
          // color: Colors.green,
          padding: const EdgeInsets.only(left: 8.0, top: 2.0, bottom: 2.0, right: 16.0),
          // decoration: BoxDecoration(borderRadius: BorderRadius.circular(50.0)),
          child: Row(
            children: <Widget>[
              Icon(Icons.add, color: Colors.white,),
              Text('Add', style: TextStyle(color: Colors.white))
            ],
          ),
        )
      ),
    );
  }

  Widget _buildTopCardFooter(BuildContext context) {
    return Container(
         color: Colors.grey[300],
//        decoration: BoxDecoration(gradient: LinearGradient(colors: [Colors.green, Colors.red,]),),
        padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('21 May 2018'),
            _roundedButton(context),
          ],
        ),
      );
  }

  //RecentTransactionCard

  Widget _buildRecentTransactionCard(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(bottom: 16.0),
      child: Column(
        children: <Widget>[
          _buildCardHeader(context, leading: 'Recent Transactions', trailing: 'May 2018'),
          Column(children: 
            _buildTransactionRows(context, _recentRecords)
          ),
          Container(
            alignment: Alignment.centerRight,
            padding: EdgeInsets.all(4.0),
            child: FlatButton(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text('More', style: TextStyle(color: Colors.grey[400], fontWeight: FontWeight.normal)),
                  RotatedBox(
                    child: Icon(Icons.chevron_right, color: Colors.grey[400],),
                    quarterTurns: 1,
                  )
                ],
              )
            ,onPressed: () => _onRecordMoreTap(context), 
            ),
          )
        ],
      )
    );
  }

  Widget _buildCardHeader(BuildContext context, {String leading = '', String trailing  = ""}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 14.0),
      color: Colors.grey[200],
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(leading, style: Theme.of(context).textTheme.title.copyWith(color: Colors.grey[400])),
          Text(trailing, style: TextStyle(color: Colors.grey[400], fontStyle: FontStyle.italic),)
        ],
      )
    );
  }

  List<Widget> _buildTransactionRows(BuildContext context, recentRecords) {
    List<Widget> items = new List<Widget>();
    recentRecords.forEach((Record r) { items.add( 
      RecordTile(r, () => _onRecordTap(context, r))); 
    });
    return items;
  }


  Widget _buildCategoryWiseSpendingCard(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(bottom: 16.0),
      child: Column(
        children: <Widget>[
          _buildCardHeader(context, leading: 'Top Spendings'),
          Column(
            children: 
            _buildTopSpendingsRows(_categories),
          ),
          Container(
            alignment: Alignment.centerRight,
            padding: EdgeInsets.all(4.0),
            child: FlatButton(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text('More', style: TextStyle(color: Colors.grey[400], fontWeight: FontWeight.normal)),
                  RotatedBox(
                    child: Icon(Icons.chevron_right, color: Colors.grey[400],),
                    quarterTurns: 1,
                  )
                ],
              )
            ,onPressed: () => _onCategoriesMoreTap(context), 
            ),
          )
        ],
      ),
    );
  }

List<ProgressTile>_buildTopSpendingsRows(List<Category> categories) {
    List<Widget> list = new List();
    return categories.take(5).map((c) {
      String catTotal = c.category_total.toStringAsFixed(0);
      if (c.category_total > 1000) {
        catTotal = (c.category_total / 1000).toStringAsFixed(1) + "K";
      }
      return ProgressTile(
          title: c.categoryName,
          percentage: c.category_total / totalSpend,
          trailing: c.category_total.toStringAsFixed(0),
          subtrailing: "${(c.category_total/totalSpend * 100).toStringAsFixed(2)}%"
        );
      }).toList();
  }
}