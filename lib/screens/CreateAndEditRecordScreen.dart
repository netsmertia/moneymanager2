import 'package:flutter/material.dart';
import 'package:moneylover2/CustomeIcons.dart';
import 'package:moneylover2/model/Category.dart';
import 'package:moneylover2/model/Record.dart';
import 'package:moneylover2/widgets/ActionTile.dart';
import 'package:moneylover2/database/Repository.dart';
import 'package:moneylover2/screens/SelectCategoryScreen.dart';
import 'package:intl/intl.dart';

class CreateAndEditRecordScreen extends StatefulWidget {

  final Repository repo; 
  final Record record;
  CreateAndEditRecordScreen(this.repo, {this.record});

  createState() => CreateAndEditRecordScreenState();
}


class CreateAndEditRecordScreenState extends State<CreateAndEditRecordScreen> {

  Record record;

  TextEditingController amoutController = TextEditingController();
  TextEditingController noteController = TextEditingController();


  initState() {
    super.initState();
    if (widget.record != null) {
      record = widget.record;
      amoutController.text = record.amount.toStringAsFixed(2);
      noteController.text = record.title;
    } else {
      record = new Record(category: new Category(), fromWallet: "Cash");
    }
    print(record.toString());
  }

  _onCalendarTap(BuildContext context)  {
    showDatePicker(context: context , 
      firstDate: DateTime(2015),
      initialDate: record.recordDate,
      lastDate: DateTime(2021),
    ).then((picked) {
      if (picked != null) {
        setState(() {
          record.recordDate = picked;
        });
      }
    });
  }

  _onCategorySelect(BuildContext context) async{
    int index = 1; 
    Category category = record.category;
    if (category?.categoryType == CategoryType.income) index = 0;
    if (category?.categoryType == CategoryType.account) index = 2;

    category = await Navigator.push(context, MaterialPageRoute(builder: (builder) {
      return SelectCategoryScreen(widget.repo, index);
    }));
    //if back button pressed in select category null will be returned so reassign the old category.
    if (category != null) {
      record.category = category;
    }
  }


  _onSaveRecord(BuildContext context) async {
    record.amount = double.parse(amoutController.text);
    record.title = noteController.text;
    if (record.amount == 0.0) {
      await _showAlert(context, content: 'Please provide the Amount');
    } else if (record.category == null || record.category.id == 0) {
      await _showAlert(context, content: 'Please select a Category');
    } else if (record.id == 0) {
      widget.repo.insertRecord(record).then((r) {
          print("record created: ${record.toString()}");
          Navigator.of(context).pop(record);
      });
    } else if (record.id != 0) {
      widget.repo.updateRecord(record).then((_) {
        print("record updated: ${record.toString()}");
        Navigator.of(context).pop(record);
      });
    }
  }

  _showAlert(BuildContext context, {String title = 'Alert !!' , String content}) async{
    return await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: new Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      }
    );
  }

  _istoday(DateTime date) {
    DateTime today = new DateTime.now();
    return date.day ==  today.day && date.month == today.month && date.year == today.year;
  }
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Transaction'),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.save), onPressed: () =>  _onSaveRecord(context),),
        ],
      ),
      body: ListView(
        children: <Widget>[
          _buildAmountTextField(),

          ActionTile(
            title: 'Note', 
            tileType: ActionTileType.input,
            leading: Icons.chat,
            controller: noteController,
          ),
          Divider(height: .5,),

          ActionTile(
            leading: Icons.date_range, 
            title: _istoday(record.recordDate) ? 'Today' : DateFormat('dd MMM 2018').format(record.recordDate),
            onTap: () => _onCalendarTap(context),
          ),

          Divider(height: .5,),
          ActionTile(
            leading: Icons.dashboard, 
            title: "Category", 
            subtitle: record.category.categoryName,
            isTwoLine: record.category.id != 0,
            onTap: () => _onCategorySelect(context),
          ),

          Divider(height: .5,),
          ActionTile(
            leading: Icons.account_balance_wallet,
            title: 'From Wallet',
            subtitle: 'Cash',
            isTwoLine: true,
            onTap: () {},
          ),
        ],
      ),
    );
  }

  Widget _buildAmountTextField() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 40.0),
      decoration: BoxDecoration(
        color: Colors.grey[300],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(CustomeIcons.rupee, size: 40.0, color: Colors.green[700],),
          SizedBox(
            width: 200.0,
              child: TextField(
                controller: amoutController,
                keyboardType: TextInputType.number,
                style: TextStyle(
                  fontSize: 30.0,
                  color: Colors.grey[700],
                ),
                decoration: InputDecoration(
                  hintText: "Enter Amount",
                  hintStyle: TextStyle(fontSize: 30.0) ,
                  border: InputBorder.none,
                ),
            )
          ),
        ],
      ),
    );
  }

}


