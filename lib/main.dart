import 'package:flutter/material.dart';
import 'package:moneylover2/screens/CreateAndEditRecordScreen.dart';
import 'package:moneylover2/screens/HomeScreen.dart';
import 'package:moneylover2/database/Repository.dart';

void main() async {
    Repository repo = new Repository();
    await repo.dbh.open();
    runApp(
      new MaterialApp(
        title: "MoneyManager",
        // home: new HomeScreen(repo),
        initialRoute: "/",
        routes: <String, WidgetBuilder> { 
          "/" : (context) => new HomeScreen(repo),
          "/createRecord" : (context) => new CreateAndEditRecordScreen(repo),
        },
        theme: ThemeData(
          primarySwatch: Colors.green,
          textSelectionColor: Colors.green[600],
        )
      )
    );
}